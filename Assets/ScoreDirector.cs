using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDirector : MonoBehaviour
{
    public GoodContoroller goodContoroller;
    GameObject score;
    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("score");
    }

    // Update is called once per frame
    public void Addscore()
    {
        int Gcounter = goodContoroller.counter;
        score.GetComponent<Text>().text = "スコア：" + Gcounter.ToString("F0") + "点";
    }
}