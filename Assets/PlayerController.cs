using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    GameObject player;
    GameObject goodPrefab;
    void Start()
    {
        player = GameObject.Find("player");
        goodPrefab = GameObject.Find("goodPrefab");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.y = 0f;
        pos.z = -36f;
        transform.position = pos;
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Translate(-0.03f, 0.0f, 0.0f);
            //this.transform.Rotate(0f, -0.1f, 0f);
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.Translate(0.03f, 0.0f, 0.0f);
            //this.transform.Rotate(0f, 0.1f, 0f);
        }
    }
}
