using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadContoroller : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.y = 0f;
        transform.position = pos;
        transform.Translate(0, 0f, -0.02f);
        if (transform.position.z < -40.0f)
        {
            Destroy(gameObject);
        }
        Vector3 p1 = transform.position;
        Vector3 p2 = this.player.transform.position;
        Vector3 dir = p2 - p1;
        float d = dir.magnitude;
        float r1 = 1.0f;
        float r2 = 2f;
        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("ScoreDirector");
            director.GetComponent<ScoreDirector>().DecreaseHp;
            Destroy(gameObject);
        }
    }
}
