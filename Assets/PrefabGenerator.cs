using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabGenerator : MonoBehaviour
{
    public GameObject badPrefab;
    public GameObject goodPrefab;
    float span = 0.5f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(badPrefab) as GameObject;
            int px = Random.Range(-20, 21);
            go.transform.position = new Vector3(px, 2, 40);
        }
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(goodPrefab) as GameObject;
            int px = Random.Range(-20, 21);
            go.transform.position = new Vector3(px, 2, 40);
        }
    }
}

